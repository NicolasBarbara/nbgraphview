//
//  ViewController.swift
//  NBGraphView
//
//  Created by NicolasBarbara on 05/17/2021.
//  Copyright (c) 2021 NicolasBarbara. All rights reserved.
//

import UIKit
import NBGraphView

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let v = view.viewWithTag(1) as! NBGraphView
        v.backgroundColor = .white
        v.label.textColor = .black
        v.items = [
            NBGraphItem(image:#imageLiteral(resourceName: "img"),score:CGFloat.random(in: 0...1)),
            NBGraphItem(image:#imageLiteral(resourceName: "img"),score:CGFloat.random(in: 0...1)),
            NBGraphItem(image:#imageLiteral(resourceName: "img"),score:CGFloat.random(in: 0...1)),
            NBGraphItem(image:#imageLiteral(resourceName: "img"),score:CGFloat.random(in: 0...1)),
            NBGraphItem(image:#imageLiteral(resourceName: "img"),score:CGFloat.random(in: 0...1))
        ]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

