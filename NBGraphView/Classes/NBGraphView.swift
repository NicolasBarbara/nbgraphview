//
//  NBGraphView.swift
//  NBGraphView
//
//  Created by Nicolas Barbara on 5/17/21.
//


import UIKit
@IBDesignable public class NBGraphView:UIView{
    @IBInspectable public var startColor:UIColor = .red
    @IBInspectable public var endColor:UIColor = .red
    @IBInspectable public var levels:Int = 5
    @IBInspectable public var totalAngle:CGFloat = 280
    @IBInspectable public var duration:CGFloat = 0.3
    @IBInspectable public var title:String = ""
    public var spacing:Int = 12
    public var backgroundsAlpha:CGFloat = 0.3
    public var centerRadius:Int = 50
    public var divinderWidth:CGFloat = 2
    public var lineWidth:CGFloat = 7
    public var items:[NBGraphItem] = []
    public let label = UILabel()
    public var imagesSize = CGSize(width: 30, height: 30)
    public override func draw(_ rect: CGRect) {
        guard items.count > 0 else {
            let colors = startColor.getColors(to: endColor, with: CGFloat(levels))
            for ii in 0...levels-1{
                let startAngle = totalAngle
                let endAngle:CGFloat = 0.0
                drawV(rect, startAngle:startAngle  , endAngle: endAngle ,radius:CGFloat(centerRadius+ii*spacing),color:colors[ii],alpha: 1)
            }
            return
        }
        let colors = startColor.getColors(to: endColor, with: CGFloat(levels))
        let number = items.count
        let bottomAngle = 360 - totalAngle
        let angleOfView:CGFloat = (360-bottomAngle)/CGFloat(number)
        for ii in 0...levels-1{
            for i in 0...number-1{
                let startAngle = CGFloat(i+1)*angleOfView
                let endAngle = CGFloat(i)*angleOfView
                drawV(rect, startAngle:startAngle  , endAngle: endAngle ,radius:CGFloat(centerRadius+ii*spacing),color:colors[ii],alpha: backgroundsAlpha)
                if ii <= Int(items[i].score * CGFloat(levels)){
                    drawV(rect, startAngle:startAngle  , endAngle: endAngle ,radius:CGFloat(centerRadius+ii*spacing),color:colors[Int(items[i].score * CGFloat(levels))],alpha: 1,beginTime:CGFloat(ii)*duration)
                }
            }
        }
        for i in 0...number{
            let angle = CGFloat(i)*angleOfView
            drawLine(rect,angle: .pi*angle/180 + .pi*(bottomAngle/2)/180)
            if i == number{
                return
            }
            let iv = UIImageView(image: items[i].image.withRenderingMode(.alwaysTemplate))
            iv.tintColor = colors[Int(items[i].score * CGFloat(levels))]
            addSubview(iv)
            iv.frame = .init(x: 0, y: 0, width: imagesSize.width, height:imagesSize.height)
            let imageAngle = ((.pi*CGFloat(i)*angleOfView/180 + .pi/2 + .pi*(bottomAngle/2)/180) + (.pi*CGFloat(i+1)*angleOfView/180 + .pi/2 + .pi*(bottomAngle/2)/180))/2
            iv.center = CGPoint.pointOnCircle(center: CGPoint(x: rect.midX, y: rect.midY), radius: rect.width/2-30, angle: imageAngle)
        }
    }
    func drawV(_ rect: CGRect,startAngle:CGFloat,endAngle:CGFloat,radius: CGFloat,color:UIColor,alpha:CGFloat,beginTime:CGFloat? = nil){
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: rect.midX, y: rect.midY), radius: radius, startAngle: .pi*startAngle/180 + .pi/2 + .pi*((360-totalAngle)/2)/180 , endAngle: .pi*endAngle/180 + .pi/2 + .pi*((360-totalAngle)/2)/180, clockwise: false)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.withAlphaComponent(alpha).cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.frame = bounds
        if beginTime != nil{
            shapeLayer.lineWidth = 0
            let animation = CABasicAnimation(keyPath: "lineWidth")
            animation.fromValue = 0.0
            animation.toValue = lineWidth
            animation.duration = CFTimeInterval(duration)
            animation.isRemovedOnCompletion = false
            animation.fillMode = CAMediaTimingFillMode.forwards
            shapeLayer.add(animation, forKey: "1")
            DispatchQueue.main.asyncAfter(deadline: .now() + Double(beginTime!)) {
                self.layer.addSublayer(shapeLayer)
                for i in 0...self.items.count{
                    let angle = CGFloat(i)*(360-(360-self.totalAngle))/CGFloat(self.items.count)
                    self.drawLine(rect,angle: .pi*angle/180 + .pi*((360-self.totalAngle)/2)/180)
                }
            }
            return
        }
        layer.addSublayer(shapeLayer)
        label.textAlignment = .center
        label.text = self.title
        self.addSubview(label)
        label.frame = CGRect(x: 0, y: rect.height-40, width: rect.width, height: 30)
    }
    func drawLine(_ rect: CGRect,angle:CGFloat){
        let linePath = UIBezierPath(rect: CGRect(x: rect.midX-1, y: rect.midY, width: 2, height: rect.height/2))
        let center = CGPoint(x: rect.midX, y: rect.midY)
        linePath.apply(CGAffineTransform(translationX: center.x, y: center.y).inverted())
        linePath.apply(CGAffineTransform(rotationAngle: angle))
        linePath.apply(CGAffineTransform(translationX: center.x, y: center.y))
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = linePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = backgroundColor?.cgColor
        shapeLayer.lineWidth = divinderWidth
        shapeLayer.frame = bounds
        layer.addSublayer(shapeLayer)
    }
    public convenience init(items:[NBGraphItem],
                            startColor:UIColor,
                            endColor:UIColor,
                            levels:Int,
                            totalAngle:CGFloat,
                            duration:CGFloat,
                            title:String){
        self.init(frame:.zero)
        self.items = items
        self.startColor = startColor
        self.endColor = endColor
        self.levels = levels
        self.totalAngle = totalAngle
        self.duration = duration
        self.title = title
    }
}
public struct NBGraphItem {
    public init(image:UIImage,score:CGFloat){
        self.image = image
        self.score = score
    }
    let image:UIImage
    let score:CGFloat
}
extension CGFloat {
    public var getPercentageValues: [CGFloat] {
        let increment: CGFloat = 100/(self-1)
        var values = [CGFloat]()
        let last: CGFloat = 100
        var value: CGFloat = 0
        while value <= last {
            values.append(value)
            value += increment
        }
        return values
    }
}

extension UIColor {
    func toColor(_ color: UIColor, percentage: CGFloat) -> UIColor {
        let percentage = max(min(percentage, 100), 0) / 100
        switch percentage {
        case 0: return self
        case 1: return color
        default:
            var (r1, g1, b1, a1): (CGFloat, CGFloat, CGFloat, CGFloat) = (.zero, .zero, .zero, .zero)
            var (r2, g2, b2, a2): (CGFloat, CGFloat, CGFloat, CGFloat) = (.zero, .zero, .zero, .zero)
            guard self.getRed(&r1, green: &g1, blue: &b1, alpha: &a1) else { return self }
            guard color.getRed(&r2, green: &g2, blue: &b2, alpha: &a2) else { return self }
            
            return UIColor(red: CGFloat(r1 + (r2 - r1) * percentage),
                           green: CGFloat(g1 + (g2 - g1) * percentage),
                           blue: CGFloat(b1 + (b2 - b1) * percentage),
                           alpha: CGFloat(a1 + (a2 - a1) * percentage))
        }
    }
    func getColors(to color: UIColor, with quantity: CGFloat) -> [UIColor] {
        quantity.getPercentageValues.map { self.toColor(color, percentage: $0 ) }
    }
}
extension CGPoint {
    static func pointOnCircle(center: CGPoint, radius: CGFloat, angle: CGFloat) -> CGPoint {
        let x = center.x + radius * cos(angle)
        let y = center.y + radius * sin(angle)
        return CGPoint(x: x, y: y)
    }
}
