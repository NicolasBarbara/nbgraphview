# NBGraphView

[![CI Status](https://img.shields.io/travis/NicolasBarbara/NBGraphView.svg?style=flat)](https://travis-ci.org/NicolasBarbara/NBGraphView)
[![Version](https://img.shields.io/cocoapods/v/NBGraphView.svg?style=flat)](https://cocoapods.org/pods/NBGraphView)
[![License](https://img.shields.io/cocoapods/l/NBGraphView.svg?style=flat)](https://cocoapods.org/pods/NBGraphView)
[![Platform](https://img.shields.io/cocoapods/p/NBGraphView.svg?style=flat)](https://cocoapods.org/pods/NBGraphView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NBGraphView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NBGraphView'
```

## Author

NicolasBarbara, n_barbara1987@hotmail.com

## License

NBGraphView is available under the MIT license. See the LICENSE file for more info.
